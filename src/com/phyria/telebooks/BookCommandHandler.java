package com.phyria.telebooks;




import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Set;


public class BookCommandHandler implements CommandExecutor {

    public Main plugin;
    SettingsManager settings = SettingsManager.getInstance();


    public BookCommandHandler(Main rpgMain) {
        this.plugin = rpgMain;

    }


    private void help(CommandSender s) {

        s.sendMessage(ChatColor.GREEN + "----------------------------------------------");
        s.sendMessage(ChatColor.RED + "Phyria teleport Commands You Can Use");
        if (s.hasPermission("Phyria.bypass")) s.sendMessage(ChatColor.GOLD + "/recall <Location>");
        s.sendMessage(ChatColor.GREEN + "----------------------------------------------");
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        Player p = (Player) sender;
        if (args.length == 0) {
            help(sender);
            return true;
        }

        if (args[0].equalsIgnoreCase("set") ) {
            if(p.hasPermission("phyria.tele.create"))
            if (args.length == 0) {
                p.sendMessage(ChatColor.RED + "Please specify a name!");
                return true;
            }
            settings.getData().set("warps." + args[1] + ".world", p.getLocation().getWorld().getName());
            settings.getData().set("warps." + args[1] + ".x", p.getLocation().getX());
            settings.getData().set("warps." + args[1] + ".y", p.getLocation().getY());
            settings.getData().set("warps." + args[1] + ".z", p.getLocation().getZ());
            settings.getData().set("warps." + args[1] + ".cost", args[2]);
            settings.saveData();
            p.sendMessage(ChatColor.GREEN + "Set warp " + args[1] + "!");
            if(plugin.warpnames.size() > 0){
                plugin.warpnames.clear();
                plugin.populateTele();
            }
            return true;
        }

        if(args[0].equalsIgnoreCase("test")){

            ItemStack book = new ItemStack(Material.BOOK);
            ItemStack ebook = new ItemStack(Material.BOOK);
            ItemStack pages = new ItemStack(Material.PAPER, 64);
            ItemMeta emeta = ebook.getItemMeta();
            ItemMeta pmeta = pages.getItemMeta();
            ItemMeta wepmeta = book.getItemMeta();
            emeta.setDisplayName(ChatColor.GREEN + "Pristine Book Of Teleportation");
            pmeta.setDisplayName("Book Page");
            wepmeta.setDisplayName(ChatColor.GREEN + "Pristine Book Of Teleportation");
            wepmeta.setLore(Arrays.asList(new String[]{
                    ChatColor.GRAY + "Untradeable",
                    ChatColor.BLUE + "50 / 50 Charges"}));
            emeta.setLore(Arrays.asList(new String[]{
                    ChatColor.GRAY + "Untradeable",
                    ChatColor.BLUE + "0 / 50 Charges"}));
            book.setItemMeta(wepmeta);
            pages.setItemMeta(pmeta);
            ebook.setItemMeta(emeta);
            p.getInventory().addItem(new ItemStack[] { pages });
            p.getInventory().addItem(new ItemStack[] { book });
            p.getInventory().addItem(new ItemStack[] { ebook });
        }
        if(args[0].equalsIgnoreCase("list")){
            Set<String> warps = settings.getData().getConfigurationSection("warps.").getKeys(false);
            String[] warpList = warps.toArray(new String[warps.size()]);
            p.sendMessage("Warps: " + org.apache.commons.lang.StringUtils.join(warpList, ' ', 0, warpList.length));

        }
        return false;
    }


}
