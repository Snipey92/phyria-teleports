package com.phyria.telebooks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.Map;

/**
 * Created by Snipey on 5/13/14.
 */
public class MainRunnable extends BukkitRunnable {

    Main plugin;

    public MainRunnable(Main main) {
        this.plugin = main;
    }

    @Override
    public void run() {


        for (Map.Entry<Player, Integer> en : plugin.recalling.entrySet()){
            Player p = en.getKey();
            if (plugin.recalling.containsKey(p)) {
                if(en.getValue() > 0){
                    plugin.recalling.put(en.getKey(), en.getValue() - 1);
                }
                if (en.getValue() == 0) {
                    p.sendMessage(plugin.prefix + "Teleporting you to " + plugin.teleloc.get(p.getName()));
                    Main.teleportPlayer(p, plugin.teleloc.get(p.getName()));
                    plugin.recalling.remove(p);
                }
                if (en.getValue() > 0) {
                    for (Location loc : this.plugin.getCircle(
                            p.getLocation(), 2.0D, 100)) {
                        ParticleEffect.WITCH_MAGIC.display(loc,
                                0.0F, 0.0F, 0.0F, 1.0F, 2);
                    }
                    for (Location loc : this.plugin.getCircle(
                            p.getLocation(), 1.0D, 100)) {
                        ParticleEffect.WITCH_MAGIC.display(loc,
                                0.0F, 0.0F, 0.0F, 1.0F, 2);
                    }
                    p.sendMessage(plugin.prefix + " Teleporting in: " + en.getValue());
                }
            }
        }

    }
}
