package com.phyria.telebooks;


import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class BookEventHandler implements Listener {


    // Instance of main plugin
    private static Main plugin;
    SettingsManager settings = SettingsManager.getInstance();
    // Constructor
    public BookEventHandler(Main levelMain) {
        this.plugin = levelMain;
    }
    @EventHandler
    public void bookOpenEvent(PlayerInteractEvent e){
        Inventory inv = Bukkit.getServer().createInventory(null, 27, "Teleport Book");
        Player p = e.getPlayer();
        String pname = p.getName();
        ItemStack air = new ItemStack(Material.AIR);
        ItemStack glasspane = new ItemStack(Material.STAINED_GLASS_PANE, 1 ,(short) 14);
        ItemMeta glass = glasspane.getItemMeta();
        glass.setDisplayName(" ");
        glasspane.setItemMeta(glass);
        if (((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) && (p.getItemInHand() != null) && (p.getItemInHand().getType() == Material.BOOK) && (!plugin.recalling.containsKey(pname))){

            plugin.openbook.put(pname, p.getItemInHand());
            for (int i = 0; i < 27; i++ ) {
                inv.setItem(i, glasspane);
            }
            for (int i = 0; i < plugin.warpnames.size(); i++ ) {
                    createDisplay(Material.PAPER, inv, i, plugin.warpnames.get(i), "Click to teleport!");
            }
            inv.setItem(18, air);
            p.openInventory(inv);

        }else if(plugin.recalling.containsKey(pname)){
            p.sendMessage(plugin.prefix + ChatColor.RED + " You are already recalling!");
        }
    }

    @EventHandler
    public void inventoryCloseEvent(InventoryCloseEvent e){
        Player p = (Player)e.getPlayer();
        Inventory inventory = e.getInventory();
        ItemStack is = p.getItemInHand();

        int charges = (int)plugin.getBookCharges(is, " Charges");
        int maxcharges = (int)plugin.getMaxBookCharges(is, " Charges");
        if(inventory.getName().equals("Teleport Book") && plugin.openbook.containsKey(e.getPlayer().getName())){
            plugin.openbook.remove(e.getPlayer().getName());
            e.getPlayer().closeInventory();
        }
        if(inventory.getName().equals("Teleport Book") && e.getInventory().getItem(18) != null){
            if(e.getInventory().getItem(18).getItemMeta().getDisplayName().contains("Book Page")){
                int amount = inventory.getItem(18).getAmount();
                int tot = amount + charges;
                if(tot > maxcharges){
                    int diff = tot - maxcharges;
                    int fin = tot - diff;
                    ItemStack paper = new ItemStack(Material.PAPER, diff);
                    ItemMeta pmeta = paper.getItemMeta();
                    pmeta.setDisplayName("Book Page");
                    paper.setItemMeta(pmeta);
                    p.getInventory().addItem(paper);
                    plugin.addBookCharges(p, fin);
                }else{
                    plugin.addBookCharges(p, amount);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        ItemStack clicked = e.getCurrentItem();
        Player p = (Player)e.getWhoClicked();
        ItemStack is = p.getItemInHand();
        Inventory inventory = e.getInventory();
        int charges = (int)plugin.getBookCharges(is, " Charges");
        if(clicked == null || clicked.getType() == Material.BOOK) return;
        if(inventory.getName().equals("Teleport Book") && clicked != null || clicked.getType() != Material.PAPER){
            if(charges != 0){
                if(!clicked.getItemMeta().getDisplayName().equalsIgnoreCase("Book Page")){
                    e.setCancelled(true);

                }else if (clicked.hasItemMeta() && clicked.getType() != Material.BOOK && clicked.getType() != Material.STAINED_GLASS_PANE && (!clicked.getItemMeta().getDisplayName().equalsIgnoreCase("Book Page"))) {
                    e.setCancelled(true);
                    plugin.teleloc.put(e.getWhoClicked().getName(), clicked.getItemMeta().getDisplayName());
                    plugin.recalling.put(p, 7);
                    e.getWhoClicked().closeInventory();
                }else if(clicked.getType() == Material.STAINED_GLASS_PANE || clicked.getType() == Material.PAPER){
                    e.setCancelled(true);
                }
            }else{
                e.setCancelled(true);
                e.getWhoClicked().closeInventory();
                p.sendMessage(plugin.prefix + ChatColor.RED + " You have no charges left");
            }
        }
    }

    @EventHandler
    public void onRecallMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        int fromX = (int)e.getFrom().getX();
        int fromY = (int)e.getFrom().getY();
        int fromZ = (int)e.getFrom().getZ();
        int toX = (int)e.getTo().getX();
        int toY = (int)e.getTo().getY();
        int toZ = (int)e.getTo().getZ();
        if (((fromX != toX) || (fromZ != toZ) || (fromY != toY)) &&
                (plugin.recalling.containsKey(p))) {
            plugin.recalling.remove(p);
            p.sendMessage(ChatColor.RED + "Recall Cancelled");
        }
    }

    @EventHandler
    public void onRecallDamager(EntityDamageByEntityEvent e) {
        if (((e.getDamager() instanceof Player)) &&
                ((e.getEntity() instanceof LivingEntity))) {
            Player p = (Player)e.getDamager();
            if (plugin.recalling.containsKey(p)) {
                plugin.recalling.remove(p);
                p.sendMessage(ChatColor.RED + "Recall Cancelled");
            }
        }
    }

    @EventHandler
    public void onRecallDamage(EntityDamageEvent e) {
        if ((e.getEntity() instanceof Player)) {
            Player p = (Player)e.getEntity();
            if (plugin.recalling.containsKey(p)) {
                plugin.recalling.remove(p);
                p.sendMessage(ChatColor.RED + "Recall Cancelled");
            }
        }
    }



    public static void createDisplay(Material material, Inventory inv, int Slot, String name, String lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        item.setItemMeta(meta);

        inv.setItem(Slot, item);
    }
}