package com.phyria.telebooks;

import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Snipey on 5/12/14.
 */
public class Main extends JavaPlugin {


    public HashMap<Player, Integer> recalling = new HashMap<Player, Integer>();

    public static HashMap<String, String> teleloc = new HashMap<String, String>();
    public HashMap<String, ItemStack> openbook = new HashMap<String, ItemStack>();
    public static HashMap<String,ArrayList<String>> warps = new HashMap<String,ArrayList<String>>();
    public static ArrayList<String> warpnames = new ArrayList<String>();




    public static String prefix = ChatColor.BLUE + "[" + ChatColor.LIGHT_PURPLE + "Teleport" + ChatColor.BLUE + "]" + ChatColor.RESET;
    public String console = "[Phyria]";

    static SettingsManager settings = SettingsManager.getInstance();

    public void onEnable(){

        System.out.println(console + " Enabling Plugin");

        System.out.println(console + " Loading Settings");
        settings.setup(this);
        registerCommands();
        System.out.println(console + " Successfully registered Commands!");
        registerEvents();
        System.out.println(console + " Registered Events");
        startRunnables();
        System.out.println(console + " Started Services");
        populateTele();
        System.out.println(console + " Plugin Loaded");
    }

    public void onDisable(){
        settings.saveConfig();
        settings.saveData();
    }

    public void registerEvents(){
        PluginManager plma = Bukkit.getServer().getPluginManager();
        plma.registerEvents(new BookEventHandler(this), this);
    }

    public void registerCommands(){
        getCommand("recall").setExecutor(new BookCommandHandler(this));
    }

    public void startRunnables(){
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new MainRunnable(this), 20L, 20L);
    }

    public ArrayList<Location> getCircle(Location center, double radius,
                                         int amount) {
        World world = center.getWorld();
        double increment = 6.283185307179586D / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        for (int i = 0; i < amount; i++) {
            double angle = i * increment;
            double x = center.getX() + radius * Math.cos(angle);
            double z = center.getZ() + radius * Math.sin(angle);
            locations.add(new Location(world, x, center.getY(), z));
        }
        return locations;
    }



    public static void teleportPlayer(Player player, String location){
        Location loc;
        ArrayList<String> warp = warps.get(location);
        if(warp != null){
            World world = Bukkit.getWorld(warp.get(3));
            double x = Double.parseDouble(warp.get(0));
            double y = Double.parseDouble(warp.get(1));
            double z = Double.parseDouble(warp.get(2));
            loc = new Location(world,x,y,z);
            player.teleport(loc);
            teleloc.remove(player.getName());
            removeBookCharges(player, Integer.valueOf(warp.get(4)));
        }
    }


    public void addBookCharges(Player p, int amount){
        ItemStack is = p.getItemInHand();
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.getLore();
        if (((String)lore.get(lore.size() - 1)).contains("Charges")) {
            int charges = getBookCharges(is, " Charges");
            int maxcharges = getMaxBookCharges(is, " Charges");

             if (charges - 1 >= 1) {
                lore.set(lore.size() - 1, ChatColor.BLUE + "" + (
                        charges + amount) + " / " + maxcharges + " Charges");
                im.setLore(lore);
                is.setItemMeta(im);
            }
        }
    }

    public static void removeBookCharges(Player p, int amount){
        ItemStack is = p.getItemInHand();
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.getLore();
        if (((String)lore.get(lore.size() - 1)).contains("Charges")) {
            int charges = getBookCharges(is, " Charges");
            int maxcharges = getMaxBookCharges(is, " Charges");

            if (charges - 1 >= 1) {
                lore.set(lore.size() - 1, ChatColor.BLUE + "" + (
                        charges - amount) + " / " + maxcharges + " Charges");
                im.setLore(lore);
                is.setItemMeta(im);
            }
        }
    }

    public static int getBookCharges(ItemStack is, String str) {
        int returnVal = 0;
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.getLore();
        if (((String)lore.get(lore.size() - 1)).contains(str)) {
            String val = ((String)lore.get(lore.size() - 1)).split(" / ")[0];
            val = val.substring(2, val.length());
            try {
                returnVal = Integer.parseInt(val);
            }
            catch (Exception localException) {
            }
        }
        return returnVal;
    }

    public static int getMaxBookCharges(ItemStack is, String str) {
        int returnVal = 0;
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.getLore();
        if (((String)lore.get(lore.size() - 1)).contains(str)) {
            String val = ((String)lore.get(lore.size() - 1)).split(" / ")[1];
            val = val.split(str)[0];
            try {
                returnVal = Integer.parseInt(val);
            }
            catch (Exception localException) {
            }
        }
        return returnVal;
    }

    public void populateTele(){
        for(String warpName : settings.getData().getConfigurationSection("warps").getKeys(false)){
            warpnames.add(warpName);
        }
        for(String warpName : settings.getData().getConfigurationSection("warps").getKeys(false)){
            ArrayList<String> warpData = new ArrayList<String>();
            warpData.add(settings.getData().getString("warps." + warpName + ".x"));
            warpData.add(settings.getData().getString("warps." + warpName + ".y"));
            warpData.add(settings.getData().getString("warps." + warpName + ".z"));
            warpData.add(settings.getData().getString("warps." + warpName + ".world"));
            warpData.add(settings.getData().getString("warps." + warpName + ".cost"));
            warps.put(warpName,warpData);
        }
    }
}